//
//  StorageService.swift
//  Coctails
//
//  Created by THXDBase on 06.09.2020.
//  Copyright © 2020 THXDBase. All rights reserved.
//

import Foundation

protocol StorageServiceType: class {
    func fetchCategories(completion: @escaping(([Category]?) -> Void))
    
}

class StorageService: StorageServiceType {
    func fetchCategories(completion: @escaping(([Category]?) -> Void)) {
        completion(nil)
    }
}
