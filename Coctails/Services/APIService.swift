//
//  APIService.swift
//  Coctails
//
//  Created by THXDBase on 06.09.2020.
//  Copyright © 2020 THXDBase. All rights reserved.
//

import Foundation
import Alamofire

enum NetworkServiceError: String, Error {
    case apiError = "API Error. Try to fetch later"
    var localizedDescription: String {
        return NSLocalizedString(self.rawValue, comment: "")
    }
}

protocol APIServiceType {
    func fetchCategories(completion: @escaping((Result<[Category], NetworkServiceError>) -> Void))
    func fetchPage(categoryName: String, completion: @escaping((Result<[Drink], NetworkServiceError>) -> Void))
}

class APIService: APIServiceType {
    func fetchCategories(completion: @escaping((Result<[Category], NetworkServiceError>) -> Void)) {
        let queue = DispatchQueue.global(qos: .userInitiated)
        let url = "https://www.thecocktaildb.com/api/json/v1/1/list.php?c=list"
        let request = AF.request(url
            , method: .get
            , parameters: nil
            , encoding: URLEncoding(destination: .queryString))
            .responseJSON(queue: queue, completionHandler: { response in
                DispatchQueue.main.async {
                    switch response.result {
                    case .success:
                        if let data = response.data {
                            do {
                                let categories = try JSONDecoder().decode(Categories.self, from: data)
                                completion(.success(categories.drinks))
                            } catch {
                                completion(.failure(.apiError))
                            }
                        }
                    case .failure:
                        completion(.failure(.apiError))
                    }
                }
            })
        request.resume()
    }
    
    func fetchPage(categoryName: String, completion: @escaping((Result<[Drink], NetworkServiceError>) -> Void)) {
        let queue = DispatchQueue.global(qos: .userInitiated)
        let url = "https://www.thecocktaildb.com/api/json/v1/1/filter.php?c=\(categoryName)"
        let urlEncoded = url.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) ?? ""
        let request = AF.request(urlEncoded
            , method: .get
            , parameters: nil
            , encoding: URLEncoding(destination: .queryString))
            .responseJSON(queue: queue, completionHandler: { response in
                DispatchQueue.main.async {
                    switch response.result {
                    case .success:
                        if let data = response.data {
                            do {
                                let drinks = try JSONDecoder().decode(Drinks.self, from: data)
                                completion(.success(drinks.drinks))
                            } catch {
                                completion(.failure(.apiError))
                            }
                        }
                    case .failure:
                        completion(.failure(.apiError))
                    }
                }
            })
        request.resume()
    }
}
