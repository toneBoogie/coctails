//
//  ListPresenter.swift
//  Coctails
//
//  Created by THXDBase on 06.09.2020.
//  Copyright © 2020 THXDBase. All rights reserved.
//

import UIKit

protocol ListViewType: class {
    func showAlert(message: String)
    func updateTable()
}

protocol ListPresenterType {
    var data: [Section] { get }
    func setupFilter()
    func fetchCategories()
    func fetchPage(isActive: Bool)
    func updateFiltersModel(model: FiltersModel?)
}

final class ListPresenter: ListPresenterType {
    weak var view: ListViewType?
    let apiService: APIServiceType!
    let router: RouterType!
    var model = FiltersModel()

    var data = [Section]()
    private var isPageFetching = false
    private let allDrinksLoaded = "No more drinks found"
    private let noSelectedFiltersMessage = "No selected filters"
    
    init(view: ListViewType, apiServise: APIServiceType, router: RouterType) {
        self.view = view
        self.apiService = apiServise
        self.router = router
    }
    
    func setupFilter() {
        router.routeToFilter(filtersModel: self.model)
    }
    
    func fetchCategories() {
        apiService.fetchCategories { [weak self] (result) in
            switch result {
            case .success(let categories):
                self?.model.addCategories(categories: categories)
                self?.fetchPage()
            case .failure(let error):
                self?.view?.showAlert(message: error.localizedDescription)
            }
        }
    }
    
    func fetchPage(isActive: Bool = true) {
        guard isPageFetching == false else {
            return
        }
        if let emptyCategory = model.emptyCategory {
            isPageFetching = true
            apiService.fetchPage(categoryName: emptyCategory.strCategory) { [weak self] (result) in
                self?.isPageFetching = false
                switch result {
                case .success(let drinks):
                    self?.model.addDrinks(emptyCategory, externalDrinks: drinks)
                    self?.data = self?.model.sections ?? [Section]()
                    self?.view?.updateTable()
                case .failure(let error):
                    self?.view?.showAlert(message: error.localizedDescription)
                }
            }
        } else {
            if model.isNoSelectedFilters {
                view?.showAlert(message: noSelectedFiltersMessage)
            } else if model.allDrinksLoaded && isActive {
                view?.showAlert(message: allDrinksLoaded)
            }
        }
    }
    
    func updateFiltersModel(model: FiltersModel?) {
        guard let model = model else {
            return
        }
        self.model = model
        self.data = model.sections 
        view?.updateTable()
    }
}
