//
//  Category.swift
//  Coctails
//
//  Created by THXDBase on 06.09.2020.
//  Copyright © 2020 THXDBase. All rights reserved.
//

import Foundation

struct Categories: Decodable {
    var drinks: [Category]
}
struct Category: Decodable {
    let strCategory: String
}

extension Category: Hashable {
    func hash(into hasher: inout Hasher) {
        hasher.combine(strCategory)
    }
}

extension Category: Equatable {
    static func == (lhs: Category, rhs: Category) -> Bool {
        return lhs.strCategory == rhs.strCategory
    }
}



