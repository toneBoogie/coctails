//
//  Drink.swift
//  Coctails
//
//  Created by THXDBase on 07.09.2020.
//  Copyright © 2020 THXDBase. All rights reserved.
//

import Foundation

struct Drinks:Decodable {
    var drinks: [Drink]
}

struct Drink: Decodable {
    let strDrink: String
    let strDrinkThumb: String
    let idDrink: String
}
