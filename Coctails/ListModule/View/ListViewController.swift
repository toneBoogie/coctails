//
//  ListViewController.swift
//  Coctails
//
//  Created by THXDBase on 06.09.2020.
//  Copyright © 2020 THXDBase. All rights reserved.
//

import UIKit

class ListViewController: UIViewController {
    
    var presenter: ListPresenterType!
    private var currentScrollPosition = CGFloat(0)
    // table view
    private let tableView = UITableView()
    // navigation bar
    lazy var customNavigationController: UIView = {
        let viewHeigth = CGFloat(70)
        let buttonSize = CGFloat(30)
        let leftHorizontalGap = CGFloat(30)
        let rightHorizontalGap = CGFloat(20)
        let view = UIView()
        view.backgroundColor = .white
        view.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: viewHeigth)
        // shadow
        view.layer.shadowColor = UIColor.black.cgColor
        view.layer.shadowOffset = CGSize(width: 0.0, height: 2.0)
        view.layer.shadowRadius = 6.0
        view.layer.shadowOpacity = 0.4
        view.layer.masksToBounds = false
        // filter button
        let button = UIButton()
        button.setImage(UIImage(named: "ico_filter"), for: .normal)
        let buttonX = view.bounds.width - buttonSize - rightHorizontalGap
        let buttonY = view.bounds.height / 2 - buttonSize/2
        button.frame = CGRect(x: buttonX, y: buttonY, width: buttonSize, height: buttonSize)
        button.addTarget(self, action: #selector(filterHandler), for: .touchUpInside)
        view.addSubview(button)
        // title
        let label = UILabel()
        label.font = UIFont(name: "Roboto-Regular", size: 24.flexible())
        label.text = "Drinks"
        label.sizeToFit()
        var labelFrame = label.bounds
        labelFrame.origin.x = leftHorizontalGap
        labelFrame.origin.y = view.bounds.height / 2 - labelFrame.size.height / 2
        label.frame = labelFrame
        label.textColor = .black
        view.addSubview(label)
        return view
    }()
    
    init() {
        super.init(nibName: nil, bundle: nil)
    }
    required init?(coder: NSCoder) {
        fatalError()
    }
    
    override func loadView() {
        view = UIView()
        view.backgroundColor = .white
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.addSubview(customNavigationController)
        let topGap = customNavigationController.bounds.height
        //
        tableView.register(UINib(nibName: "ListCell", bundle: nil), forCellReuseIdentifier: "ListCell")
        tableView.separatorStyle = .none
        tableView.delegate = self
        tableView.dataSource = self
        tableView.contentInset = UIEdgeInsets(top: topGap, left: 0, bottom: 0, right: 0)
        tableView.scrollIndicatorInsets = UIEdgeInsets(top: topGap, left: 0, bottom: 0, right: 0)
        view.addSubview(tableView)
        view.sendSubviewToBack(tableView)
        //
        presenter.fetchCategories()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        presenter.fetchPage(isActive: false)
    }
    
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        tableView.frame = view.bounds
    }
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    @objc
    func filterHandler(_ sender: UIBarButtonItem) {
        presenter.setupFilter()
    }
}

extension ListViewController: ListViewType {
    func showAlert(message: String) {
        tableView.tableFooterView = nil
        let alert = UIAlertController(title: message, message: nil, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Ok", style: .default))
        self.present(alert, animated: true)
    }
    func updateTable() {
        tableView.tableFooterView = nil
        tableView.reloadData()
    }
}

extension ListViewController: UITableViewDelegate {
    
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        currentScrollPosition = scrollView.contentOffset.y
    }
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let position = scrollView.contentOffset.y
        guard currentScrollPosition < position else {
            return
        }
        if position > tableView.contentSize.height - scrollView.frame.size.height - 100 {
            tableView.tableFooterView = createSpinnerView()
            presenter.fetchPage(isActive: true)
        }
    }
    
    private func createSpinnerView()-> UIView {
        let view = UIView(frame: CGRect(x: 0, y: 0, width: self.view.bounds.width, height: 100))
        let spinner = UIActivityIndicatorView()
        spinner.startAnimating()
        spinner.center = view.center
        view.addSubview(spinner)
        return view
    }
}

extension ListViewController: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return presenter.data.count
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let view = UIView(frame: CGRect(x: 0, y: 0, width: self.tableView.bounds.width, height: 36))
        view.backgroundColor = .white
        let label = UILabel()
        label.font = UIFont(name: "Roboto-Regular", size: 14.flexible())
        label.textColor = UIColor(hex: "#7E7E7E")
        label.text = presenter.data[section].title
        label.sizeToFit()
        var labelFrame = label.bounds
        labelFrame = CGRect(x: 20, y: 10, width: labelFrame.width, height: labelFrame.height)
        label.frame = labelFrame
        view.addSubview(label)
        return view
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 36
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 140
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return presenter.data[section].drinks.count 
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "ListCell") as? ListCell else {
            return UITableViewCell()
        }
        cell.data = presenter.data[indexPath.section].drinks[indexPath.row]
        return cell
    }
    
    
}
