//
//  ListCell.swift
//  Coctails
//
//  Created by THXDBase on 07.09.2020.
//  Copyright © 2020 THXDBase. All rights reserved.
//

import UIKit

class ListCell: UITableViewCell {
    
    @IBOutlet weak var drinkTitle: UILabel!
    @IBOutlet weak var drinkImageView: UIImageView!
    var data: Drink? {
        willSet{
            if newValue?.idDrink != data?.idDrink {
                drinkImageView.alpha = 0
                drinkImageView.image = nil
            }
        }
        didSet{
            drinkTitle.text = data?.strDrink ?? ""
            if let url = data?.strDrinkThumb {
                drinkImageView.setImageFrom(urlString: url)
                UIView.animate(withDuration: 0.3) { [weak self] in
                    self?.drinkImageView.alpha = 1
                }
            }
        }
    }

}
