//
//  AssemblyBuilder.swift
//  Coctails
//
//  Created by THXDBase on 06.09.2020.
//  Copyright © 2020 THXDBase. All rights reserved.
//

import Foundation

protocol AssemblyBuilderType: class {
    func createListModule(router: RouterType) -> ListViewController
    func createFiltersModule(router: RouterType, model: FiltersModel) -> FiltersViewController
}

class AssemblyBuilder: AssemblyBuilderType {
    func createListModule(router: RouterType) -> ListViewController {
        let view = ListViewController()
        let apiService = APIService()
        let presenter = ListPresenter(view: view, apiServise: apiService, router: router)
        view.presenter = presenter
        return view
    }
    
    func createFiltersModule(router: RouterType, model: FiltersModel) -> FiltersViewController {
        let view = FiltersViewController()
        let presenter = FiltersPresenter(view: view, model: model, router: router)
        view.presenter = presenter
        return view
    }
    
}
