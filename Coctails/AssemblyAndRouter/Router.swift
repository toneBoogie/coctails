//
//  Router.swift
//  Coctails
//
//  Created by THXDBase on 06.09.2020.
//  Copyright © 2020 THXDBase. All rights reserved.
//

import UIKit

protocol RouterTypeBase: class {
    var navigationController: UINavigationController { get set }
    var builder: AssemblyBuilderType { get set }
    init(navigationController: UINavigationController, builder: AssemblyBuilderType)
}

protocol RouterType: RouterTypeBase {
    func routeToList()
    func routeToFilter(filtersModel: FiltersModel)
    func routeFromFilter(filtersModel: FiltersModel?)
}

class Router: RouterType {
    var navigationController: UINavigationController
    var builder: AssemblyBuilderType
    
    required init(navigationController: UINavigationController, builder: AssemblyBuilderType) {
        self.navigationController = navigationController
        self.builder = builder
    }
    
    func routeToList() {
        let vc = builder.createListModule(router: self)
        navigationController.viewControllers = [vc]
    }
    
    func routeToFilter(filtersModel: FiltersModel) {
        let vc = builder.createFiltersModule(router: self, model: filtersModel)
        navigationController.pushViewController(vc, animated: true)
    }
    
    func routeFromFilter(filtersModel: FiltersModel?) {
        if filtersModel != nil, let vc = navigationController.viewControllers.first as? ListViewController {
            vc.presenter?.updateFiltersModel(model: filtersModel)
        }
        navigationController.popViewController(animated: true)
    }
}
