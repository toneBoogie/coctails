//
//  FiltersPresenter.swift
//  Coctails
//
//  Created by THXDBase on 06.09.2020.
//  Copyright © 2020 THXDBase. All rights reserved.
//

import Foundation

protocol FiltersViewType:class {
    func updateTable()
}

protocol FiltersPresenterType: class {
    var data: [Filter] { get }
    var model: FiltersModel { get set }
    init(view: FiltersViewType, model: FiltersModel, router: RouterType)
    
    func show()
    func back(withSave: Bool)
    func updateFilter(flag: Bool, name: String) 
}

class FiltersPresenter: FiltersPresenterType {
    weak var view: FiltersViewType?
    var model: FiltersModel
    let router: RouterType!
    
    var data: [Filter] {
        return model.filters
    }

    required init(view: FiltersViewType, model: FiltersModel, router: RouterType) {
        self.view = view
        self.model = model
        self.router = router
    }
    
    func show() {
        view?.updateTable()
    }
    
    func back(withSave: Bool){
        let param = withSave ? model : nil
        router.routeFromFilter(filtersModel: param)
    }
    
    func updateFilter(flag: Bool, name: String) {
        model.setSelection(flag, categoryName: name)
        view?.updateTable()
    }
}
