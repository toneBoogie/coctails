//
//  FiltersViewController.swift
//  Coctails
//
//  Created by THXDBase on 06.09.2020.
//  Copyright © 2020 THXDBase. All rights reserved.
//

import UIKit

class FiltersViewController: UIViewController {
    var presenter: FiltersPresenterType!
    // table view
    private let tableView = UITableView()
    private let applyButtonHeight = CGFloat(53)
    
    lazy var customNavigationController: UIView = {
        let viewHeigth = CGFloat(70)
        let buttonSize = CGFloat(30)
        let leftButtonHorizontalGap = CGFloat(20)
        let leftLabelHorizontalGap = CGFloat(89)
        let view = UIView()
        view.backgroundColor = .white
        view.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: viewHeigth)
        // shadow
        view.layer.shadowColor = UIColor.black.cgColor
        view.layer.shadowOffset = CGSize(width: 0.0, height: 2.0)
        view.layer.shadowRadius = 6.0
        view.layer.shadowOpacity = 0.4
        view.layer.masksToBounds = false
        // filter button
        let button = UIButton()
        button.setImage(UIImage(named: "ico_arrow"), for: .normal)
        let buttonX = leftButtonHorizontalGap
        let buttonY = view.bounds.height / 2 - buttonSize/2
        button.frame = CGRect(x: buttonX, y: buttonY, width: buttonSize, height: buttonSize)
        button.addTarget(self, action: #selector(backHandler), for: .touchUpInside)
        view.addSubview(button)
        // title
        let label = UILabel()
        label.font = UIFont(name: "Roboto-Regular", size: 24.flexible())
        label.text = "Drinks"
        label.sizeToFit()
        var labelFrame = label.bounds
        labelFrame.origin.x = leftLabelHorizontalGap
        labelFrame.origin.y = view.bounds.height / 2 - labelFrame.size.height / 2
        label.frame = labelFrame
        label.textColor = .black
        view.addSubview(label)
        return view
    }()
    
    lazy var applyButton: UIButton = {
       let button = UIButton()
        button.setTitle("APPLY", for: .normal)
        button.titleLabel?.font = UIFont(name: "Roboto-Bold", size: 16)
        button.setTitleColor(.white, for: .normal)
        button.backgroundColor = UIColor(hex: "#272727")
        button.addTarget(self, action: #selector(apply), for: .touchUpInside)
        return button
    }()
    
    init() {
        super.init(nibName: nil, bundle: nil)
    }
    required init?(coder: NSCoder) {
        fatalError()
    }
    
    override func loadView() {
        view = UIView()
        view.backgroundColor = .white
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        view.addSubview(customNavigationController)
        let topGap = customNavigationController.bounds.height
        //
        tableView.register(UINib(nibName: "CategoryCell", bundle: nil), forCellReuseIdentifier: "CategoryCell")
        tableView.separatorStyle = .none
        tableView.delegate = self
        tableView.dataSource = self
        tableView.contentInset = UIEdgeInsets(top: topGap, left: 0, bottom: 0, right: 0)
        tableView.scrollIndicatorInsets = UIEdgeInsets(top: topGap, left: 0, bottom: 0, right: 0)
        view.addSubview(tableView)
        view.sendSubviewToBack(tableView)
        //
        view.addSubview(applyButton)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        presenter.show()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        applyButton.frame = CGRect(x: 27
            , y: view.bounds.height - applyButtonHeight - 30
            , width: view.bounds.width - 27 * 2
            , height: applyButtonHeight)
        
        var tableFrame = view.bounds
        tableFrame.size.height = applyButton.frame.minY
        tableView.frame = tableFrame
    }
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    @objc
    func backHandler(_ sender: UIButton) {
        presenter?.back(withSave: false)
    }
    
    @objc
    func apply(_ sender: UIButton) {
         presenter?.back(withSave: true)
    }
    
}


extension FiltersViewController: FiltersViewType {
    func updateTable() {
        tableView.reloadData()
    }
}

extension FiltersViewController: UITableViewDelegate {
    
}

extension FiltersViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 81
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return presenter.data.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "CategoryCell") as? CategoryCell else {
            return UITableViewCell()
        }
        cell.data = presenter.data[indexPath.row]
        cell.onCheck = { [weak self] (flag, name) in
            self?.presenter.updateFilter(flag: flag, name: name)
        }
        return cell
    }
    
    
}
