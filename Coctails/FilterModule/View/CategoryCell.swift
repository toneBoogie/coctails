//
//  CategoryCell.swift
//  Coctails
//
//  Created by THXDBase on 07.09.2020.
//  Copyright © 2020 THXDBase. All rights reserved.
//

import UIKit

class CategoryCell: UITableViewCell {

    @IBOutlet weak var checkButton: UIButton!
    @IBOutlet weak var categoryTitle: UILabel!
    
    var onCheck: ((Bool, String) -> Void)?

    var data: Filter? {
        didSet{
            guard data != nil else {
                categoryTitle.text = ""
                checkButton.imageView?.image = nil
                return
            }
            categoryTitle.text = data?.title ?? ""
            let image = data!.isSelected ? UIImage(named: "ico_check") : nil
            checkButton.setImage(image, for: .normal)
        }
    }
    
    @IBAction func onCheckHandler(_ sender: Any) {
        guard let data = self.data else {
            return
        }
        onCheck?(!data.isSelected, data.title)
    }
}
