//
//  FiltersModel.swift
//  Coctails
//
//  Created by THXDBase on 06.09.2020.
//  Copyright © 2020 THXDBase. All rights reserved.
//

import Foundation

// Model for persistance
struct StoredCategoryValue {
    let index: Int
    var drinks = [Drink]()
    var isSelected = true
}

// Model for List
struct Section {
    let title: String
    let drinks: [Drink]
}

// Model for Filters
struct Filter {
    let title: String
    var isSelected = true
}

// MARK: - main model

struct FiltersModel {
    private var drinks = [Category:StoredCategoryValue]()
    // for List
    var sections: [Section]{
         drinks
            .filter({ $0.value.drinks.isEmpty == false && $0.value.isSelected })
            .sorted(by: { $0.value.index < $1.value.index })
            .map({ Section(title: $0.key.strCategory, drinks: $0.value.drinks) })
    }
    // for Filters
    var filters: [Filter] {
        drinks
            .sorted(by: { $0.value.index < $1.value.index })
            .map({ Filter(title: $0.key.strCategory, isSelected: $0.value.isSelected) })
    }
    
    var emptyCategory: Category? {
        drinks
            .filter({ $0.value.isSelected })
            .sorted(by: { $0.value.index < $1.value.index })
            .first(where: { $0.value.drinks.isEmpty })?
            .key
    }
    
    var isNoSelectedFilters: Bool {
        return drinks.isEmpty == false && drinks.filter({ $0.value.isSelected }).isEmpty
    }
    
    var allDrinksLoaded: Bool {
        return drinks.filter({ $0.value.isSelected }).count <= drinks.filter({ $0.value.drinks.isEmpty == false  }).count
    }
    
    mutating func addCategories(categories:[Category]) {
        for (index,category) in categories.enumerated() {
            drinks[category] = StoredCategoryValue(index: index)
        }
    }
    
    mutating func addDrinks(_ toCategory: Category, externalDrinks: [Drink]) {
        drinks[toCategory]?.drinks.append(contentsOf: externalDrinks)
    }
    
    mutating func setSelection(_ flag: Bool, categoryName: String) {
        drinks[Category(strCategory: categoryName)]?.isSelected = flag
    }
}
