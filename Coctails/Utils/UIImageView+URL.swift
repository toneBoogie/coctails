//
//  UIImageView+URL.swift
//  Coctails
//
//  Created by THXDBase on 07.09.2020.
//  Copyright © 2020 THXDBase. All rights reserved.
//

import UIKit
import Alamofire

fileprivate let imageCache = NSCache<NSString, UIImage>()

extension UIImageView {
    func setImageFrom(urlString: String) {
        if let imageFromCache = imageCache.object(forKey: urlString as NSString) {
            self.image = imageFromCache
        } else {
            let loader = UIActivityIndicatorView(style: .medium)
            addSubview(loader)
            loader.translatesAutoresizingMaskIntoConstraints = false
            loader.centerXAnchor.constraint(equalTo: centerXAnchor).isActive = true
            loader.centerYAnchor.constraint(equalTo: centerYAnchor).isActive = true
            loader.startAnimating()
            AF.request(urlString).responseData { response in
                if let data = response.data, let image = UIImage(data: data) {
                    imageCache.setObject(image, forKey: urlString as NSString)
                    loader.stopAnimating()
                    loader.removeFromSuperview()
                    self.image = image
                }
            }
        }
    }
}
